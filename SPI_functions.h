/*
  MIT License

  Copyright (c) 2018 Mina Helmi

  Permission is hereby granted, free of charge, to any person obtaining
  a copy of this software and associated documentation files (the "Software"),
  to deal in the Software without restriction, including without limitation
  the rights to use, copy, modify, merge, publish, distribute, sublicense,
  and/or sell copies of the Software, and to permit persons to whom the
  Software is furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be
  included in all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
  OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
  DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
  ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#ifndef MCAL_DRIVERS_SPI_DRIVER_SPI_FUNCTIONS_H_
#define MCAL_DRIVERS_SPI_DRIVER_SPI_FUNCTIONS_H_


#include "../../basic_includes/custom_types.h"
#include "../DIO_driver/DIO_functions.h"

// (master/slave) data order, LSB first or MSB first
typedef enum
{
    SPI_data_order_MSB_first,
    SPI_data_order_LSB_first
} SPI_data_order_t;

// master pre-scaler factor
typedef enum
{
    SPI_prescaler_div_4   = 0b00000000,
    SPI_prescaler_div_16  = 0b00000001,
    SPI_prescaler_div_64  = 0b00000010,
    SPI_prescaler_div_128 = 0b00000011
} SPI_prescaler_master_t;

// clock polarity
typedef enum
{
    SPI_CPOL_0,
    SPI_CPOL_1
} SPI_clock_polarity_t;

// clock phase
typedef enum
{
    SPI_CPHA_0,
    SPI_CPHA_1
} SPI_clock_phase_t;

// slave pin ~SS
typedef struct
{
    DIO_port_t portSlave;
    u8 pinSlave;

} SPI_slave_obj_t;

typedef const SPI_slave_obj_t* SPI_slave_cptr_t;

extern const SPI_slave_cptr_t SPI_SS_default; // default ~SS pin (pin B4)

typedef void (*SPI_CB_slave_t)(u8);    // callback for slave
typedef void (*SPI_CB_master_t)(void); // callback for master

// --- Slave --- //
void SPI_vidSlaveInitSS(SPI_slave_cptr_t structPtrSlaveCpy);
void SPI_vidSlaveInit(void);

u8 SPI_u8SlaveReceive(void);
void SPI_vidSlaveSetData(const u8 u8DataCpy);
// ------------- //

// --- Master --- //
void SPI_vidMasterInitSS(SPI_slave_cptr_t structPtrSlaveCpy);
void SPI_vidMasterInit(SPI_prescaler_master_t enumPrescalerCpy);

u8 SPI_u8MasterTransmitByte(const u8 u8DataCpy, SPI_slave_cptr_t structPtrSlaveCpy);
void SPI_vidMasterTransmitStr(const char* charPtrStrCpy, SPI_slave_cptr_t structPtrSlaveCpy);
void SPI_vidMasterTransmitArray(const u8* u8ArrayCpy, const u16 u16LengthCpy, SPI_slave_cptr_t structPtrSlaveCpy);

u8 SPI_u8MasterRead(SPI_slave_cptr_t structPtrSlaveCpy, const u8 u8isReTransmitCpy);
// -------------- //

// --- Setup --- //
void SPI_vidDisable(void);

void SPI_vidMasterSetClock(SPI_prescaler_master_t enumPrescalerCpy);
void SPI_vidSetDataOrder(SPI_data_order_t enumDataOrderCpy);
void SPI_vidMasterIs2xClock(const u8 u8is2xCpy);
void SPI_vidSetClockPolarity(SPI_clock_polarity_t enumClkPolarityCpy);
void SPI_vidSetClockPhase(SPI_clock_phase_t enumClkPhaseCpy);
// ------------- //

// --- interrupts --- //
void SPI_vidEnableINT(void);
void SPI_vidDisableINT(void);

void SPI_vidRegisterCB_slave(const SPI_CB_slave_t CBfuncCpy);
void SPI_vidDeregisterCB_slave(void);

void SPI_vidRegisterCB_master(const SPI_CB_master_t CBfuncCpy);
void SPI_vidDeregisterCB_master(void);
// ------------------ //


#endif /* MCAL_DRIVERS_SPI_DRIVER_SPI_FUNCTIONS_H_ */

