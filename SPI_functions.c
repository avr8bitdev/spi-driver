/*
  MIT License

  Copyright (c) 2018 Mina Helmi

  Permission is hereby granted, free of charge, to any person obtaining
  a copy of this software and associated documentation files (the "Software"),
  to deal in the Software without restriction, including without limitation
  the rights to use, copy, modify, merge, publish, distribute, sublicense,
  and/or sell copies of the Software, and to permit persons to whom the
  Software is furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be
  included in all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
  OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
  DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
  ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include "SPI_functions.h"
#include "../Peripherals_addresses.h"
#include "../../basic_includes/bit_manip.h"
#include <avr/interrupt.h>
#include <util/delay.h>


// --- internal --- //
#define SPI_NSS  4
#define SPI_MOSI 5
#define SPI_MISO 6
#define SPI_SCK  7

#define SPI_MASTER_DELAY_us 50

// mode Slave or Master
typedef enum
{
    SPI_mode_slave,
    SPI_mode_master
} SPI_mode_t;

SPI_CB_slave_t SPI_CB_slave   = 0; // slave callback
SPI_CB_master_t SPI_CB_master = 0; // master callback

// default SS pin
static const SPI_slave_obj_t SPI_slave_default = { port_B, SPI_NSS };

// this is made internal since enabling SPI requires configuration
static inline void SPI_vidEnable(void)
{
    BIT_SET(SPI_CTRL, 6);
}

// set slave or master
static inline void SPI_vidSet_mode(SPI_mode_t enumModeCpy)
{
    BIT_ASSIGN(SPI_CTRL, 4, enumModeCpy);
}

static inline u8 SPI_u8GetDataAndClearFlags(void)
{
    // interrupt flag and write collision flag are both cleared by doing the following:
    // 1) first reading the Status Register
    // 2) then accessing the Data Register
    volatile u8 status_reg = SPI_STATUS;
    status_reg++; // TODO: this suppresses compiler warning, find another way

    return SPI_DATA;
}

static inline void SPI_vidWaitWhileBusy(void)
{
    // interrupt flag becomes 1 when data is received
    while (!BIT_GET(SPI_STATUS, 7)) // while interrupt flag = 0
    {}
}

// give slave some time to process the received byte
inline static void SPI_vidMasterDelay(void)
{
    _delay_us(SPI_MASTER_DELAY_us);
}
// ---------------- //

// pointer to default SS pin
const SPI_slave_cptr_t SPI_SS_default = &SPI_slave_default;

// --- Slave --- //
inline void SPI_vidSlaveInitSS(SPI_slave_cptr_t structPtrSlaveCpy)
{
    DIO_vidSet_pinDirection(structPtrSlaveCpy->portSlave, structPtrSlaveCpy->pinSlave, INPUT);
}

void SPI_vidSlaveInit(void)
{
    // When the SPI is enabled as a slave,
    // this pin is configured as an input regardless of the setting of DDB7.
    DIO_vidSet_pinDirection(port_B, SPI_SCK, INPUT); // pin SCK (clock) (B7)

    DIO_vidSet_pinDirection(port_B, SPI_MISO, OUTPUT); // pin MISO (B6)
    DIO_vidSet_pinValue(port_B, SPI_MISO, OFF);

    // When the SPI is enabled as a slave,
    // this pin is configured as an input regardless of the setting of DDB5.
    DIO_vidSet_pinDirection(port_B, SPI_MOSI, INPUT); // pin MOSI (B5)

    // When the SPI is enabled as a Slave,
    // this pin is configured as an input regardless of the setting of DDB4.
    SPI_vidSlaveInitSS(SPI_SS_default); // pin ~SS (B4)

    SPI_vidSetDataOrder(SPI_data_order_MSB_first);
    SPI_vidSetClockPolarity(SPI_CPOL_0);
    SPI_vidSetClockPhase(SPI_CPHA_0);
    SPI_vidSet_mode(SPI_mode_slave);

    SPI_vidEnable();
}

u8 SPI_u8SlaveReceive(void)
{
    SPI_vidWaitWhileBusy();

    return SPI_u8GetDataAndClearFlags();
}

inline void SPI_vidSlaveSetData(const u8 u8DataCpy)
{
    SPI_u8GetDataAndClearFlags();

    SPI_DATA = u8DataCpy;
}
// ------------- //

// --- Master --- //
inline void SPI_vidMasterInitSS(SPI_slave_cptr_t structPtrSlaveCpy)
{
    DIO_vidSet_pinDirection(structPtrSlaveCpy->portSlave, structPtrSlaveCpy->pinSlave, OUTPUT);
    DIO_vidSet_pinValue(structPtrSlaveCpy->portSlave, structPtrSlaveCpy->pinSlave, ON); // don't select the slave
}

void SPI_vidMasterInit(SPI_prescaler_master_t enumPrescalerCpy)
{
    DIO_vidSet_pinDirection(port_B, SPI_SCK, OUTPUT); // pin SCK (clock) (B7)
    DIO_vidSet_pinValue(port_B, SPI_SCK, OFF);

    // When the SPI is enabled as a Master,
    // this pin is configured as an input regardless of the setting of DDB6.
    // When the pin is forced by the SPI to be an input,
    // the pull-up can still be controlled by the PORTB6 bit.
    DIO_vidSet_pinDirection(port_B, SPI_MISO, INPUT); // pin MISO (B6)

    DIO_vidSet_pinDirection(port_B, SPI_MOSI, OUTPUT); // pin MOSI (B5)
    DIO_vidSet_pinValue(port_B, SPI_MOSI, OFF);

    SPI_vidMasterInitSS(SPI_SS_default); // pin ~SS (B4)

    SPI_vidMasterSetClock(enumPrescalerCpy);
    SPI_vidMasterIs2xClock(0);
    SPI_vidSetDataOrder(SPI_data_order_MSB_first);
    SPI_vidSetClockPolarity(SPI_CPOL_0);
    SPI_vidSetClockPhase(SPI_CPHA_0);
    SPI_vidSet_mode(SPI_mode_master);

    SPI_vidEnable();
}

u8 SPI_u8MasterTransmitByte(const u8 u8DataCpy, SPI_slave_cptr_t structPtrSlaveCpy)
{
    // Master initiates the communication cycle when pulling-low
    // the SS pin of the desired slave.
    DIO_vidSet_pinValue(structPtrSlaveCpy->portSlave, structPtrSlaveCpy->pinSlave, OFF); // select the slave

    // writing a byte to the Data Register starts the SPI clock generator,
    // and the hardware shifts the 8-bits into the slave.
    // After shifting is done, the clock generator stops, setting the interrupt Flag.
    SPI_DATA = u8DataCpy;

    SPI_vidWaitWhileBusy();

    // clear interrupt and collision flags
    register u8 slave_data = SPI_u8GetDataAndClearFlags();

    // give slave some time to process the received byte
    SPI_vidMasterDelay();

    // signal the end of packet by pulling-high the SS line.
    DIO_vidSet_pinValue(structPtrSlaveCpy->portSlave, structPtrSlaveCpy->pinSlave, ON); // don't select the slave

    return slave_data;
}

void SPI_vidMasterTransmitStr(const char* charPtrStrCpy, SPI_slave_cptr_t structPtrSlaveCpy)
{
    // Master initiates the communication cycle when pulling-low
    // the SS pin of the desired slave.
    DIO_vidSet_pinValue(structPtrSlaveCpy->portSlave, structPtrSlaveCpy->pinSlave, OFF); // select the slave

    for (; *charPtrStrCpy; charPtrStrCpy++)
    {
        // writing a byte to the Data Register starts the SPI clock generator,
        // and the hardware shifts the 8-bits into the slave.
        // After shifting is done, the clock generator stops, setting the interrupt Flag.
        SPI_DATA = *charPtrStrCpy;

        SPI_vidWaitWhileBusy();

        // clear interrupt and collision flags
        SPI_u8GetDataAndClearFlags();

        // give slave some time to process the received byte
        SPI_vidMasterDelay();
    }

    // signal the end of packet by pulling-high the SS line.
    DIO_vidSet_pinValue(structPtrSlaveCpy->portSlave, structPtrSlaveCpy->pinSlave, ON); // don't select the slave
}

void SPI_vidMasterTransmitArray(const u8* u8ArrayCpy, const u16 u16LengthCpy, SPI_slave_cptr_t structPtrSlaveCpy)
{
    // Master initiates the communication cycle when pulling-low
    // the SS pin of the desired slave.
    DIO_vidSet_pinValue(structPtrSlaveCpy->portSlave, structPtrSlaveCpy->pinSlave, OFF); // select the slave

    for (u16 i = 0; i < u16LengthCpy; i++, u8ArrayCpy++)
    {
        // writing a byte to the Data Register starts the SPI clock generator,
        // and the hardware shifts the 8-bits into the slave.
        // After shifting is done, the clock generator stops, setting the interrupt Flag.
        SPI_DATA = *u8ArrayCpy;

        SPI_vidWaitWhileBusy();

        // clear interrupt and collision flags
        SPI_u8GetDataAndClearFlags();

        // give slave some time to process the received byte
        SPI_vidMasterDelay();
    }

    // signal the end of packet by pulling-high the SS line.
    DIO_vidSet_pinValue(structPtrSlaveCpy->portSlave, structPtrSlaveCpy->pinSlave, ON); // don't select the slave
}

u8 SPI_u8MasterRead(SPI_slave_cptr_t structPtrSlaveCpy, const u8 u8isReTransmitCpy)
{
    // send dummy byte, master will receive slave data via MISO inside register 'SPI_DATA'
    SPI_u8MasterTransmitByte(0, structPtrSlaveCpy);

    // save received slave data
    register u8 slave_data = SPI_DATA;

    if (u8isReTransmitCpy)
        SPI_u8MasterTransmitByte(slave_data, structPtrSlaveCpy); // re-transmit original slave data

    return slave_data;
}
// -------------- //

// --- Setup --- //
inline void SPI_vidDisable(void)
{
    BIT_CLEAR(SPI_CTRL, 6);
}

void SPI_vidMasterSetClock(SPI_prescaler_master_t enumPrescalerCpy)
{
    BIT_ASSIGN( SPI_CTRL, 1, BIT_GET(enumPrescalerCpy, 1) );
    BIT_ASSIGN( SPI_CTRL, 0, BIT_GET(enumPrescalerCpy, 0) );
}

inline void SPI_vidSetDataOrder(SPI_data_order_t enumDataOrderCpy)
{
    BIT_ASSIGN(SPI_CTRL, 5, enumDataOrderCpy);
}

inline void SPI_vidMasterIs2xClock(const u8 u8is2xCpy)
{
    BIT_ASSIGN(SPI_STATUS, 0, u8is2xCpy);
}

inline void SPI_vidSetClockPolarity(SPI_clock_polarity_t enumClkPolarityCpy)
{
    BIT_ASSIGN(SPI_CTRL, 3, enumClkPolarityCpy);
}

inline void SPI_vidSetClockPhase(SPI_clock_phase_t enumClkPhaseCpy)
{
    BIT_ASSIGN(SPI_CTRL, 2, enumClkPhaseCpy);
}
// ------------- //

// --- interrupts --- //
inline void SPI_vidEnableINT(void)
{
    BIT_SET(SPI_CTRL, 7);
}

inline void SPI_vidDisableINT(void)
{
    BIT_CLEAR(SPI_CTRL, 7);
}

inline void SPI_vidRegisterCB_slave(const SPI_CB_slave_t CBfuncCpy)
{
    SPI_CB_slave = CBfuncCpy;
}

inline void SPI_vidDeregisterCB_slave(void)
{
    SPI_CB_slave = 0;
}

inline void SPI_vidRegisterCB_master(const SPI_CB_master_t CBfuncCpy)
{
    SPI_CB_master = CBfuncCpy;
}

inline void SPI_vidDeregisterCB_master(void)
{
    SPI_CB_master = 0;
}
// ------------------ //


// called when data is transmitted (master),
// or data is received (slave)
ISR(SPI_STC_vect) // serial transfer complete
{
    register u8 data_reg = SPI_u8GetDataAndClearFlags();

    switch (BIT_GET(SPI_CTRL, 4)) // get current mode (slave or master)
    {
        case SPI_mode_slave:
            if (SPI_CB_slave)
                SPI_CB_slave(data_reg);
        break;

        case SPI_mode_master:
            if (SPI_CB_master)
                SPI_CB_master();
        break;
    }
}

